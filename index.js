
// Writing comments in Javascript:
// There are two ways of writing comments in JS:
	// - single line comments - cmd + /
	/*
		multi line comments:
		cmd + opt + /
	*/

console.log("Hello World");

/*

	Javascript - we can see or log message in our console.

	Consoles are part of our browsers which allow to see/log messages, data or information from our programming language -- Javascript.

	Console can be accessed through its developer tools in the console tab.

	Consoles in browsers allow to add some Javascript expressions.

	Statements are instructions, expressions we add to our programming language which will then be communicated to our computers.

	Statements in javascript commonly ends in semicolon(;). However, javascript has an implemented way of automatically adding semicolons at the end of our statements. Which, therefore, mean that, unlike other languages, JS does NOT require semicolons.

	Semicolons in JS are mostly used to mark the end of the statement.

	Syntax
	Syntax in programming, is a set of rules that describes how statements are properly made/constructed.

	Lines/blocks of code must follow a certain set of rules for it to work.

*/

console.log("Efren Rodriguez Jr.")  //log this message on the console.


// Variables

/*
	In HTML, elements are containers of other elements and text.
	In Javascript, Variables are containers of data. Give name to describe a piece of data.

	Variables also allow us to use or refer to data multiple times.

*/

//num is our variable
//10 being the value/data
let num = 10;

console.log(6);
console.log(num);

let name1 = "Jeffrey"

console.log("John")
console.log(name1)

// Creating Variables

/* To create a variable, there are 2 steps to be done:
	-Declaration which allows to create the variable.
	-Initialization which allows to add an initial value to a variable.

	Variables in JS are declared with the use of let or const keyword.

*/

let myVariable;
/*we can create variables without an initial value. However, when logged into the console, the variable will return a value of undefined.

	Undefined is a data type that indicates that variable does exist however there was no initial value.
	
	*/

console.log(myVariable);


/*
	You can always initialize a variable after declaration by assigning a value to the variable with the use of the assignment operator (=)	
*/

myVariable = "New initialized Value"

console.log(myVariable);


/* you cannot and should not access a variable before it's been created/declared.*/
/*myVariable2 = "Initial Value";
let myVariable2;
console.log(myVariable2);*/


/* can you use or refer to a variable that has not been declared or created?
	No, this will result in error: Not Defined.

	Undefined vs Not Defined:
	Undefined - means a variable has been declared but there is no initial value.
		- Undefined is a data type
	Not Defined - means a variable you are trying to access does not exist.
		- Not defined is an error.
	
	Some errors in js will stop the program from further executing.
*/

// console.log(sumVariable);
// Uncaught ReferenceError: sumVariable is not defined

let myVariable3 = "Another sample"
console.log(myVariable3);

/*
	Variables must be declared first before they are used, referred to, or accessed.
*/

// Let vs Const
/*
	With the use of let, we can create variables that can be declared, initialized and re-assigned. We can declare let variables and initialize after.
*/

let bestFinalFantasy;
bestFinalFantasy = "Final Fantasy 6";
console.log(bestFinalFantasy);
// Reassigning let variables

bestFinalFantasy = "Final Fantasy 7";
console.log(bestFinalFantasy);

/*let bestFinalFantasy = "Final Fantasy 10"
console.log(bestFinalFantasy);*/

// Const - const variables are variables with constant data. Therefore we should not re-declare, re-assign or even declare a const variable without initialization.

// can you declare a const var without init? No.
/*const pi;
pi=3.1416;
console.log(pi);*/

// can you reassign another value to a const variable? No.
const goat = "Michael Jordan";
console.log(goat);
/*goat = "Lebron James";
console.log(goat);*/

/*
	const variable are used for data that we expect or do not want its values to changes.
*/

// Guides on Variable Names
/*
	1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

		let firstName = "Michael"
	
	2. when naming variable names, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in js that starts in capital letter,

		let firstName = "Michael"

	3. Do not add spaces to your variable names. Use camelCase for multiple words or underscores.

*/

// Declaring multiple variables

	let brand = "Toyota", model = "Vios", type = "Sedan";
	console.log(brand);
	console.log(model);
	console.log(type);

/* to create data with particular data types some data types require adding with a literal.

	string literals = '', "" and most recently: `` (template literals)
	object literals = {}
	array literals = []

*/

// String

	let country = 'Philippines';
	let province = "Metro Manila";

	console.log(country);
	console.log(province);


	let firstName, lastName;
	firstName = "Efren Jr.";
	lastName = "Rodriguez";
	console.log(firstName, lastName);

	/* Concatenation is a process wherein we combine two strings as one.*/
	console.log(firstName+ " " + lastName);
	let fullName = firstName+" "+lastName;
	console.log(fullName);

	let word1 = "is";
	let word2 = "student";
	let word3 = "of";
	let word4 = "School";
	let word5 = "Zuitt Coding";
	let word6 = "a";
	let word7 = "Institute";
	let word8 = "Bootcamp";
	let space = " ";

/*	let sentence = 
	firstName+
	space+
	word1+
	space+
	word6+
	space+
	word2+
	space+
	word3+
	space+
	word5+
	space+
	word8;*/


	/*Template literals (``) allows to create string with  use of backticks. Allows to easily concatenate string without use of + (plus). Allows embedding or adding of variable and even expressions in our string with the use of placeholders ${}.*/

	let sentence = `${fullName} ${word1} ${word6} ${word2} ${word3} ${word5} ${word8}`;

	console.log(sentence);

	let numString1 = "5"
	let numString2 = "6"
	let num1 = 5
	let num2 = 6
	let num3 = 5.5
	let num4 = .5

	// force coercion = one data typs is forced to change to complete an operation
	console.log(numString1+num1); // "55"
	console.log(num3+numString2); // "5.56"

	// parseInt() - convert numeric string to proper number
	console.log(num4+parseInt(numString1)) //5.5

	let sum2 = num1 + parseInt(numString1) //10
	console.log(sum2)


// Mathematical Operations (-,*,/,%)

	// subtraction
	/*
		let numString1 = "5"
		let numString2 = "6"
		let num1 = 5
		let num2 = 6
		let num3 = 5.5
		let num4 = .5
	*/
	console.log(num1-num3) //-0.5
	console.log(num3-num4) // 5
	console.log(numString1-num2) //-1 results in proper mathematical operation because string was forced to become a number. 
	console.log(numString2-num2) //0
	console.log(numString1-numString2) //-1
	// in substraction, numeric strings will not concatenate
	let sample2 = "Efren Jr"
	console.log(sample2 - numString1) // NaN - results in not a number. When trying to perform subtraction between alphanumeric string and numeric string, the result is NaN

	// Multiplication
	console.log(num1*num2)//30
	console.log(numString1*num1)//25
	console.log(numString1*numString2)//30

	let product = num1*num2//30
	let product2 = numString1*num1//25
	let product3 = numString1*numString2//30

	// Division
	console.log(product/num2)
	console.log(product2/5)
	console.log(numString2/numString1)

	// Division/Multiplication by 0
	console.log(product2*0)//0
	console.log(product3/0)//Infinity

	// %Modulo - remainder of a division operation
	console.log(product2%num2)// 25/6 = remainder 1
	console.log(product3%product2)//30/25 = remainder 5
	console.log(num1%num2)//5/6 = remainder 5
	console.log(num1%num1)//5/5 = remainder 0
	console.log(product2%num1)//25/5 = remainder 0
	console.log(product3%num2)//30/6 = remainder 0

// Boolean (true or false)
	// Boolesn is used for logic operations or for if-else conditions
	// When creating a variable which will contain boolean, the variable name is usually a yes or no question.

	let isAdmin = true;
	let isMarried = false;
	let isMVP = true;

	console.log("Is she married?: " + isMarried);
	console.log("Is he the MVP? " + isMVP);
	console.log(`Is he the current admin? ` + isAdmin);

// Arrays
	// Arrays are a special kind of data  type used to store multiple values.
	// Arrays can store data with different types BUT as the best practice, arrays are used to contain multiple values of the same data type
	// values in array are separated by commas
	// an array is created with an Array Literal []

	let array1 = ["Goku","Piccolo","Gohan","Vegeta"]
	// console.log(array1)
	let array2 = ["One Punch Man",true,500,"Saitama"]
	// console.log(array2)

	// in future sessions, we will be introduced to array methods which will allow us to manipulate our arrays. Having an array with differing data types may obstruct from the proper use of these methods. If we want to group differing data types we can use objects instead.

	// Arrays are better thought of as groups of data.

// Objects
	//Objects are another special kind of data type used to mimic real world objects
	//Used to create complex data that contain pieces of information that are relevant to each other.
	// Objects are created with object literals {}
	// Each data/value is paired with a key.
	// Each field is called a property.
	// Each field is separated by a comma.

	let hero1 = {

		heroName: "One Punch Man",
		isActive: true,
		salary: 500,
		realname: "Saitama",
		height: 200,
	}

	console.log(hero1.height);

	let band = ["Tom Morello","Brad Wilk","Tim Commerford", "Zack De La Rocha"];
	console.log(band[0]);

	let person = {
		firstName: "Efren",
		lastName: "Rodriguez",
		isDeveloper: true,
		hasPortfolio: true,
		age: 16
	};
	console.log(person.hasPortfolio);

// Undefined vs Null

	// Null - is explicit absence of data. This is done to project that a variable contains nothing, over undefined, as undefined merely means there is no data in the variable because the variable has not been assigned an initial value.

	let sampleNull = null;

	// Undefined - is a representation that a variable has been declared but it was not assigned an initial value.

	let sampleUndefined;

	console.log(sampleNull);
	console.log(sampleUndefined);


	let foundResult = null;

	let person2 = {
		name: "Peter",
		age: 35
	}
	console.log(person2.isAdmin)


// Functions
	// Functions in JS, are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked.
	// Functions are reusable pieces of code with instructions which used over and over again just so long as we can call/invoke them.

	/*
		Syntax:

		function functionName(){

			//code block, the block of code that will be executed once the function has been run/called/invoked.

		}
	*/

	function printName(){

		console.log("My Name is Efren Jr");

	};

	// Invoke a function
	// functionName()
	printName();
	printName();

	function showSum(){

		console.log(6+6);

	};


	showSum();
	showSum();
	showSum();

	// Note: do not create functions with the same name
	// Parameters and Arguments
	// "name" is called a parameter.
	// A parameter acts as a named variable/container that exists only inside of the function. This is used to store information to act as a stand-in or the container to the value passed into the function as an argument.

	function printName(name){

		console.log(`My name is ${name}`);

	};

	// you cannot get the value of a parameter outside of its function.
	// console.log(name);

	printName("Jeff");

	function displayNum(number){
		console.log(number);
	}

	displayNum(5000);

	// Mini-activity

	function showMessage(message){
		console.log(message);
	}

	let sampleMessage = "Javascript is Fun";
	showMessage(sampleMessage);

	// Multiple Parameters and Arguments
		// A function can not only receive a single argument but it can also receive multiple arguments so long as it matches the number of parameters in the function.

		function displayFullName(firstName,lastName,age){
			console.log(`${firstName} ${lastName} is ${age}`);
		}

		displayFullName("Efren Jr.","Rodriguez",16);

// return keyword
	
	/*
		return keyword is used so that a function may return a value.
		it also stops the process of the function. Any other instruction after keyword will not be processed.
	*/

	function createFullName(firstName,middleName,lastName){
		return `${firstName} ${middleName} ${lastName}`;
		console.log("I will no longer run because the function's value/result has been returned.")
	}
	//result of this function can be saved in to a variable
	//the result of a function without a return keyword will not save the result in a variale.

	let fullName1 = createFullName("Tom","Cruise","Mapother");
	let fullName2 = displayFullName("William","Bradley","Pitt");

	console.log(fullName1);
	console.log(fullName2);

	let fullName3 = createFullName("Jeffrey","John","Smith");
	console.log(fullName3);


